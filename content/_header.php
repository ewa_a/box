<!DOCTYPE html>

<html>
    
    <head>
        
        <meta charset="UTF-8">
        <link href="bootstrap/bootstrap.min.css" rel="stylesheet">
        
        <link rel="stylesheet" href="content/css/style.css" type="text/css"/>
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
        
        <title>ech! ech!</title>
        
    </head>
    
    <body>

        <div>
            <div class="ewadiv">
            <nav aria-label="...">
            <ul class="pager">
              <li><a href="?page=mainpage">Home</a></li>
              <li><a href="?page=blog">Blog</a></li>
              <li><a href="?page=about">About</a></li>
              <li><a href="?page=kontakt">Kontakt</a></li>
            </ul>
          </nav>
            </div>
        </div>

        
<!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>   