
<div class="jumbotron jumbotron-sm">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-sm-12">
                <h1 class="h1">
                    Cześć! <small>Napisz do mnie</small></h1>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="well well-sm">
                <form>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">
                                imię</label>
                                <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span>
                                </span>
                            <input type="text" class="form-control" id="name" placeholder="imię" required="required" /></div>
                        </div>
                        <div class="form-group">
                            <label for="email">
                                email</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
                                </span>
                                <input type="email" class="form-control" id="email" placeholder="email" required="required" /></div>
                        </div>
                        <div class="form-group">
                            <label for="subject">
                                telefon</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-phone"></span>
                                </span>
                                <input type="phone" class="form-control" id="phone" placeholder="numer telefonu" required="required" /></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">
                                wiadomość</label>
                            <textarea name="message" id="message" class="form-control" rows="9" cols="25" required="required"
                                placeholder="wiadomość"></textarea>
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-warning pull-right">Wyślij <span class="glyphicon glyphicon-send"></span></button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

